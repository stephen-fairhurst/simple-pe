include:
  - project: computing/gitlab-ci-templates
    file:
      - conda.yml
      - python.yml

stages:
  - build
  - basic
  - GW150914
  - pipeline
  - docs
  - deploy

.parallel_wvf: &parallel_wvf
  parallel:
    matrix:
      - WVF: ["IMRPhenomXHM", "IMRPhenomXPHM", "IMRPhenomTPHM"]

tarball:
  extends:
    - .python:build
  stage: build
  needs: []

conda:
  extends:
    - .conda:base
  stage: build
  needs: []
  script:
    - conda create --name py310 python=3.10
    - conda activate py310
    - conda install -c conda-forge pip2conda
    - python -m pip2conda --output environment.txt
    - conda install --file environment.txt
    - python -m pip install .

style:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:flake8
    - .python:flake8
  stage: basic
  needs: []
  variables:
    # don't fail the pipeline because of linting issues,
    # these are presented in the code-quality box in the
    # merge_request UI
    FLAKE8_OPTIONS: "--exit-zero"

executables:
  stage: basic
  image: python:3.10
  needs:
    - tarball
  before_script:
    - python -m pip install simple_pe*.tar.*
    - python -m pip install .[plotting,pipe]
  script:
    - simple_pe_analysis --help
    - simple_pe_convert --help
    - simple_pe_corner --help
    - simple_pe_datafind --help
    - simple_pe_filter --help
    - simple_pe_gracedb --help
    - simple_pe_hm_amp --help
    - simple_pe_horizon --help
    - simple_pe_localization_distribution --help
    - simple_pe_localization_ellipses --help
    - simple_pe_pipe --help
    - simple_pe_sky_coverage --help

.metric:
  stage: GW150914
  image: python:3.10
  needs:
    - tarball
    - executables
  before_script:
    - python -m pip install .[pipe]
    - python -m pip install simple_pe*.tar.*
  script:
    - cd examples/GW150914
    - sed -i "s/approximant=IMRPhenomXPHM/approximant=${WVF}/g" config.ini
    - if [ "${WVF}" = "IMRPhenomTPHM" ]; then 
        sed -i "s/trigger_parameters=trigger_parameters.json/trigger_parameters=trigger_param_aligned.json/g" config.ini;
        fi
    - simple_pe_pipe config.ini
    - bash ./outdir/submit/bash_simple_pe.sh
    - mv ./outdir/output/peak_parameters.json ../../${WVF}_GW150914_peak_parameters.json
    - mv ./outdir/output/peak_snrs.json ../../${WVF}_GW150914_peak_snrs.json
    - mv ./outdir/output/corner.png ../../${WVF}_GW150914_corner.png
    - cd ../..
  artifacts:
    paths:
        - ${WVF}_GW150914_peak_parameters.json
        - ${WVF}_GW150914_peak_snrs.json
        - ${WVF}_GW150914_corner.png

.injections:
  stage: pipeline
  extends: .metric
  script:
    - cd examples/zero-noise
    - sed -i "s/approximant=IMRPhenomXPHM/approximant=${WVF}/g" config.ini
    - simple_pe_pipe config.ini
    - bash ./outdir/submit/bash_simple_pe.sh
    - mv ./outdir/output/peak_parameters.json ../../${WVF}_peak_parameters.json
    - mv ./outdir/output/peak_snrs.json ../../${WVF}_peak_snrs.json
    - mv ./outdir/output/corner.png ../../${WVF}_injection_corner.png
    - cd ../..
  artifacts:
    paths:
        - ${WVF}_peak_parameters.json
        - ${WVF}_peak_snrs.json
        - ${WVF}_injection_corner.png

fixed_localization:
  extends: .metric
  variables:
    WVF: "IMRPhenomXPHM"
  before_script:
    - python -m pip install .[pipe]
    - python -m pip install simple_pe*.tar.*
    - LOC='"ra":2.2, "dec":-1.2, "psi":0.7'
    - sed -i "s/}/, ${LOC}}/g" examples/GW150914/trigger_parameters.json
  artifacts:

analytic_localization:
  <<: *parallel_wvf
  extends: .metric

bayestar_localization:
  extends: .metric
  variables:
    WVF: "IMRPhenomXPHM"
  before_script:
    - python -m pip install .[pipe]
    - python -m pip install simple_pe*.tar.*
    - curl https://dcc.ligo.org/public/0122/P1500227/012/bayestar_gstlal_C01.fits.gz -o bayestar_gstlal_C01.fits.gz
    - gunzip bayestar_gstlal_C01.fits.gz
    - mv bayestar_gstlal_C01.fits examples/GW150914
    - echo "localization_file=bayestar_gstlal_C01.fits" >> examples/GW150914/config.ini
  artifacts:

grid_peak_finder:
  extends: .metric
  variables:
    WVF: "IMRPhenomXPHM"
  before_script:
    - python -m pip install .[pipe]
    - python -m pip install simple_pe*.tar.*
    - echo "peak_finder=grid" >> examples/GW150914/config.ini
  artifacts:

injections:zero-noise:
  extends: .injections
  variables:
    WVF: "IMRPhenomXPHM"

localization:
  stage: pipeline
  image: python:3.10
  needs:
    - tarball
    - executables
  before_script:
    - python -m pip install .[plotting,pipe]
    - python -m pip install simple_pe*.tar.*
  script:
    - cd examples
    - simple_pe_localization_ellipses --net-state design --outdir outdir
    - simple_pe_sky_coverage --net-state design --outdir outdir
    - simple_pe_localization_distribution --net-state design --outdir outdir

    - mv ./outdir/H1L1V1_design_sky_ellipses_found.png ../H1L1V1_design_sky_ellipses_found.png
    - mv ./outdir/H1L1V1_design_sky_sens.png  ../H1L1V1_design_sky_sens.png
    - mv ./outdir/H1L1V1_design_2nd_pol.png ../H1L1V1_design_2nd_pol.png
    - mv ./outdir/H1L1V1_design_localization_hist.png  ../H1L1V1_design_localization_hist.png 

    - cd ../..
  artifacts:
    paths:
        - H1L1V1_design_sky_ellipses_found.png
        - H1L1V1_design_sky_sens.png
        - H1L1V1_design_2nd_pol.png  
        - H1L1V1_design_localization_hist.png 

documentation:
  stage: docs
  image: python:3.10
  needs:
    - tarball
    - analytic_localization
    - injections:zero-noise
    - localization
  before_script:
    - python -m pip install .[docs]
    - python -m pip install simple_pe*.tar.*
  script:
    - mkdir -p docs/source/images
    - mv IMRPhenomXPHM_GW150914_corner.png docs/source/images
    - mv IMRPhenomTPHM_GW150914_corner.png docs/source/images
    - mv IMRPhenomXPHM_injection_corner.png docs/source/images
    - mv H1L1V1_design_sky_ellipses_found.png docs/source/images
    - mv H1L1V1_design_sky_sens.png docs/source/images
    - mv H1L1V1_design_2nd_pol.png docs/source/images
    - cd docs && make html
  artifacts:
    paths:
      - docs/build/html/

pages:
  stage: deploy
  image: python:3.10-slim
  dependencies:
    - conda
    - tarball
    - documentation
  script:
    - mkdir public/
    - mv docs/build/html/* ./public/
  artifacts:
    paths:
    - public
  rules:
    - if: '$CI_PROJECT_PATH == "stephen.fairhurst/simple-pe" && $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
