#! /usr/bin/env python

import numpy as np
import pylab as plt
import os
import argparse
from pesummary.core.cli.actions import DictionaryAction
from pycbc import waveform, strain, psd

from simple_pe import localization
from simple_pe import detectors


def command_line():
    """Define the command line arguments for `simple_pe_localization_distribution`
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--seed",
        help="random seed to set for reproducibility",
        default=123456789,
        type=int
    )
    parser.add_argument(
        "--net-state",
        help="the network state (one of those defined in detectors.py)",
        default=None,
        type=str,
    )
    parser.add_argument(
        "--psd",
        help=(
            "PSD specification for each ifo to use for the analysis. "
            "Available options given by psd.get_lalsim_psd_list(). "
            "Must be provided as a space separated dictionary,"
            "e.g. H1:aLIGOaLIGODesignSensitivityT1800044 "
            "L1:aLIGOaLIGODesignSensitivityT1800044 V1:AdvVirgo"
        ),
        nargs="+",
        default={},
        action=DictionaryAction,
    )
    parser.add_argument(
        "--f_low",
        help=(
            "low frequency cutoff to be used for each ifo. Must be provided as "
            "a space separated dictionary, e.g. H1:20 L1:20"
        ),
        nargs="+",
        default={},
        action=DictionaryAction,
    )
    parser.add_argument(
        "--loc-methods",
        help=(
            "localization method."
            "Any of ('time', 'coh', 'left', 'right', 'marg')"
        ),
        default=["time", "coh"],
        type=str,
        nargs="*",
    )
    parser.add_argument(
        "--found-thresh",
        help="Threshold at which signal is observed in single detector",
        default=5.0,
        type=float,
    )
    parser.add_argument(
        "--net-thresh",
        help="Network threshold for detection",
        default=12.0,
        type=float,
    )
    parser.add_argument(
        "--loc-thresh",
        help="Threshold for a detector to contribute to "
             "localization",
        default=4.0,
        type=float,
    )
    parser.add_argument(
        "--duty-cycle",
        help="Duty cycle for each detector (assumed equal and independent "
             "for all detectors)",
        default=1.,
        type=float,
    )
    parser.add_argument(
        "--nevents",
        help="Number of events to simulate",
        default=int(1e5),
        type=int,
    )
    parser.add_argument(
        "--max-area",
        help="Maximum area to consider",
        default=50,
        type=float,
    )
    parser.add_argument(
        "--outdir",
        help="Directory to store the output",
        default="./",
    )

    return parser


def main(args=None):
    """Main interface for `simple_pe_localization_distribution`
    """
    parser = command_line()
    opts, _ = parser.parse_known_args(args=args)
    np.random.seed(opts.seed)

    if not os.path.isdir(opts.outdir):
        os.mkdir(opts.outdir)

    # generate network
    n = detectors.Network(threshold=opts.net_thresh)
    if opts.net_state and opts.psd:
        raise ValueError(
            "Please specify either the net state or the psds, not both"
            )
    elif opts.net_state:
        n.set_configuration(opts.net_state,
                            opts.found_thresh,
                            opts.loc_thresh,
                            opts.duty_cycle)
    elif opts.psd:
        ifos = opts.psd.keys()
        if not opts.f_low or (opts.f_low.keys() != ifos):
            raise ValueError(
                "Must specify f_low for each detector that has a psd"
                )
        psds = {}
        for ifo in ifos:
            opts.f_low[ifo] = float(opts.f_low[ifo])
            m1 = m2 = 1.4
            f_high = 4096
            length = strain.strain.next_power_of_2(
                int(waveform.compress.rough_time_estimate(m1, m2,
                float(opts.f_low[ifo]))))
            psds[ifo] = psd.analytical.from_string(opts.psd[ifo], 
                                                   length * f_high + 1, 
                                                   1/length,
                                                   opts.f_low[ifo])
        approximant = "IMRPhenomD"
        n.generate_network_from_psds(ifos, psds, opts.f_low, approximant,
                                     opts.found_thresh, opts.loc_thresh, 
                                     opts.duty_cycle)
    else:
        raise ValueError(
            "Print please specify either the net state or the psds"
            )

    # set up trials
    d_h = max(n.get_data("horizon"))
    d_max = d_h * 8 / (opts.net_thresh / np.sqrt(len(n.ifos)))

    num_found = 0
    num_loc = 0

    localize_list = []
    found_list = []
    full_list = []
    for trial in range(opts.nevents):
        x = localization.Event.random_values(d_max)
        x.add_network(n)
        full_list.append(x)
        if x.detected:
            num_found += 1
            if x.localized >= 3:
                num_loc += 1
                x.localize_all(methods=opts.loc_methods)
                localize_list.append(x)
            else:
                found_list.append(x)

    area = {}
    patches = {}

    for loc in opts.loc_methods:
        area[loc] = np.array([x.area[loc] for x in localize_list])
        patches[loc] = np.array([x.patches[loc] for x in localize_list])

    n_on = {}
    n_on['found'] = []
    for x in found_list + localize_list:
        n_on['found'].append( sum([hasattr(x, ifo) for ifo in n.ifos]) )
    n_on['found'] = np.asarray(n_on['found'])

    n_on['loc'] = []
    for x in localize_list:
        n_on['loc'].append( sum([hasattr(x, ifo) for ifo in n.ifos]) )
    n_on['loc'] = np.asarray(n_on['loc'])

    n_found = np.asarray([x.found for x in (found_list + localize_list)])
    n_loc = np.asarray([x.localized for x in localize_list])

    # write summary output
    fstart = "".join(np.sort(n.ifos))
    if opts.net_state:
        fstart += "_" + opts.net_state
    fname = "%s/%s_localization_output.txt" % (opts.outdir, fstart)
    f = open(fname, "w")

    if opts.net_state:
        f.write('network state: %s\n' % opts.net_state)
    else:
        f.write('sensitivities given by')
        for ifo in n.ifos:
            f.write('%s: %s' % (ifo, opts.psd[ifo]))
        f.write("\n")
    f.write("----------------------\n")
    f.write("Detectors: %s\n" % ", ".join(n.ifos))
    for ifo in n.ifos:
        i = getattr(n, ifo)
        f.write("\n")
        f.write("For %s\n" % ifo)
        f.write("------\n")
        for detail in ["horizon", "f_mean", "f_band", "duty_cycle"]:
            f.write("%s = %.2e\n" % (detail, getattr(i, detail)))

    f.write("\nLocalization results:\n")
    f.write("We have simulated %d events, uniform in volume to a distance of %d MPc\n" % 
            (opts.nevents, d_max) )
    f.write("Assuming an independent duty cycle of %.1f for each detector\n"
            % opts.duty_cycle)
    f.write("%d events were found (2 detectors SNR above %.1f, network above %1.f)\n"
        % (num_found, opts.found_thresh, opts.net_thresh))
    for i in np.arange(2, len(n.ifos) + 1):
        f.write("Had %d detectors on for %d events\n" % (i, sum(n_on['found'] == i)) )
        f.write("Found in %d detectors for %d found events\n" % (i, sum(n_found == i)) )    
    f.write("-------------------\n")
    f.write("%d were localized (3+ detectors SNR above %.1f)\n" % (num_loc, opts.loc_thresh))
    for i in range(3, len(n.ifos) + 1):
        f.write("Had %d detectors on for %d localized events\n" % (i, sum(n_on['loc'] == i)) )
        f.write("Localized in %d detectors for %d localized events\n" % (i, sum(n_loc == i)) )    
    for method in opts.loc_methods:
        f.write("---------------------\n")
        f.write("Localization by %s:\n" % method)
        f.write("Median area %.1f deg^2\n" % np.median(area[method]))
        f.write("2 patches for %d percent; 1 patch for %d percent of sources\n"
            % (100. * sum(patches[method] == 2) / len(patches[method]),
               100. * sum(patches[method] == 1) / len(patches[method])))
        for a in [1,5,20]:
            nf = sum(area[method] < a)
            f.write("Found %d sources (%.1f%% of found) within %d deg^2\n" % 
                  (nf, 100. * nf / (len(found_list + localize_list)), a )  )
 
    f.close()

    plt.figure(figsize=(10,10))
    plt.subplot(211)
    plt.grid()
    plt.xlim([0, opts.max_area])
    plt.ylabel("Probability Density")

    plt.subplot(212)
    plt.grid()
    plt.xlabel("Localization (Square Degrees)")
    plt.ylabel("Cumulative Probability")
    plt.xlim([0, opts.max_area])
    plt.ylim([0,1])

    a_step = 0.5  # step size
    a_window = 2.5  # smoothing size
    w = np.hanning(2 * a_window / a_step)
    bins = np.arange(0, opts.max_area + 4 * a_window, a_step)
    sky_ring = (opts.max_area + 2 * a_window) * np.ones(len(found_list))
    colors = {'time': 'b', 'coh': 'r', 'marg': 'k'}
    labels = {'time':'Timing', 'coh':'Coherent', 'marg':'Astro priors', 3:'3 Detectors', 4:'4 Detectors', 5:'5 Detectors'}
    sty = {'all': 'solid', 3:'dashed', 4: 'dashdot', 5:'dotted'}
    
    for method in opts.loc_methods: 
        # append the found (with large area)
        area_found = np.concatenate((area[method], sky_ring))   
        # make histogram and smooth it
        h, b = np.histogram(np.minimum(area_found, opts.max_area + 2 * a_window), bins, density=True)
        y = np.convolve(w/w.sum(), h, 'same')
        b = 0.5*(b[:-1] + b[1:])
        y = np.insert(y[b <= (opts.max_area + a_window)], 0, 0)
        b = np.insert(b[b <= (opts.max_area + a_window)], 0, 0)

       # plot pdf
        plt.subplot(211)
        plt.plot(b, y, color=colors[method], label=labels[method])

        # and cumulative
        plt.subplot(212)
        plt.plot(b, np.cumsum(y) * a_step,  
               color=colors[method], label=labels[method])
        plt.legend(loc='best')
        plt.savefig("%s/%s_localization_hist.png" %
                    (opts.outdir, fstart),
                    dpi=200, bbox_inches='tight')

if __name__ == "__main__":
    main()



