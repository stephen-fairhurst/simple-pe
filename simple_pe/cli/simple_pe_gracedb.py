#! /usr/bin/env python

import argparse
from bilby_pipe.gracedb import (
    read_from_gracedb, extract_psds_from_xml, _read_cbc_candidate,
    CHANNEL_DICTS
)
import configparser
import logging
from pesummary.gw.conversions import component_masses_from_mchirp_q
import os
from .simple_pe_pipe import get_gid
from . import _logger_format

__author__ = [
    "Charlie Hoy <charlie.hoy@ligo.org>",
]

# modify existing bilby_pipe logger to match simple-pe
logger = logging.getLogger("bilby_pipe")
logger.name = "simple-pe"
logger.handlers[0].setFormatter(
    logging.Formatter(_logger_format(), datefmt='%Y-%m-%d  %H:%M:%S')
)


def command_line():
    """
    Define the command line arguments for `simple_pe_gracedb`
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--gid",
        help=(
            "GraceDB ID for the event you wish to analyse. If "
            "provided, and --trigger_parameters is not provided, the "
            "trigger parameters are downloaded from the best matching "
            "search template on GraceDB. There is no need to provide both "
            "--sid and --gid; --gid will be used if provided."
        )
    )
    parser.add_argument(
        "--sid",
        help=(
            "superevent ID for the event you wish to analyse. If "
            "provided, and --trigger_parameters is not provided, the "
            "trigger parameters are downloaded from the best matching "
            "search template on GraceDB."
        )
    )
    parser.add_argument(
        "--channel-dict",
        type=str,
        default="online",
        choices=list(CHANNEL_DICTS.keys()),
        help=(
            "Channel dictionary. \n"
            " online   : use for main GraceDB page events from the current "
            "observing run (default)\n"
            " o2replay : use for playground GraceDB page events\n"
            " o3replay : use for playground GraceDB page events\n"
            " gwosc    : use for events where the strain data is publicly "
            "available, e.g., previous observing runs\n"
        ),
    )
    parser.add_argument(
        "--outdir",
        help="Directory to store the output",
        default="./",
    )
    return parser


def _dict_to_config_string(dd):
    """Convert a dictionary to a string which is compatible with a
    simple-pe configuration file

    Parameters
    ----------
    dd: dict
        dictionary you wish to convert
    """
    return "{" + ",".join([f"{key}:{item}" for key, item in dd.items()]) + "}"


def main(args=None):
    """Main interface for `simple_pe_gracedb`
    """
    parser = command_line()
    opts, _ = parser.parse_known_args(args=args)
    _outdir = f"{opts.outdir}/gracedb"
    os.makedirs(_outdir, exist_ok=True)
    gid = get_gid(sid=opts.sid, gid=opts.gid)
    candidate = read_from_gracedb(
        gid, "https://gracedb.ligo.org/api/", _outdir
    )
    trigger_values, _, trigger_time, ifos, _, _ = _read_cbc_candidate(
        candidate
    )
    mass1, mass2 = component_masses_from_mchirp_q(
        trigger_values.pop("chirp_mass"), trigger_values.pop("mass_ratio")
    )
    trigger_values.update(
        {"mass_1": mass1, "mass_2": mass2, "time": trigger_time, "chi_p": 0.2}
    )
    if candidate.get("coinc_file", None) is not None:
        psd_dict, psd_maximum_frequency = extract_psds_from_xml(
            coinc_file=candidate["coinc_file"], ifos=ifos, outdir=_outdir,
        )

    config = configparser.ConfigParser()
    config["pesummary"] = {
        "trigger_time": trigger_time,
        "channels": _dict_to_config_string(
            {
                ifo: ch for ifo, ch in CHANNEL_DICTS[opts.channel_dict].items()
                if ifo in ifos
            }
        ),
        "outdir": opts.outdir,
        "f_low": 20.,
        "f_high": 2048.,
        "trigger_parameters": _dict_to_config_string(trigger_values),
        "approximant": "IMRPhenomXPHM",
        "psd": _dict_to_config_string(psd_dict),
        "minimum_data_length": 256,
        "snr_threshold": 3,
        "peak_finder": "grid",
        "convert_samples": True,
        "accounting_group": "ligo.dev.o4.cbc.pe.lalinference",
        "accounting_group_user": os.environ.get("USER", "albert.einstein"),
    }
    with open(f'{opts.outdir}/config.ini', 'w') as configfile:
        config.write(configfile)
    print(f"\nTo analyse jobs run: simple_pe_pipe {opts.outdir}/config.ini\n")


if __name__ == "__main__":
    main()
