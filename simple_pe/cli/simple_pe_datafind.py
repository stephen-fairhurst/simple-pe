#! /usr/bin/env python

import os
import copy
import argparse
from gwpy.timeseries import TimeSeries
import json
import numpy as np
from simple_pe.param_est import SimplePESamples
from simple_pe.waveforms import waveform_modes
from simple_pe import io
from pesummary.core.cli.actions import DictionaryAction
from . import logger

__author__ = [
    "Charlie Hoy <charlie.hoy@ligo.org>",
]


def command_line():
    """
    Define the command line arguments for `simple_pe_datafind`
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--outdir",
        help="Directory to store the output",
        default="./",
    )
    parser.add_argument(
        "--gaussian_noise",
        help="Inject signals into gaussian-noise",
        action="store_true"
    )
    parser.add_argument(
        "--channels",
        help=(
            "Channels to use when reading in strain data. Must be provided as "
            "a space separated dictionary with keys giving the ifo and items "
            "giving the channel name, e.g. H1:HWINJ_INJECTED. For GWOSC open "
            "data the dictionary items must be GWOSC, e.g. H1:GWOSC. If you "
            "wish to use simple-pe to produce an injection for you, the "
            "dictionary items must be INJ, e.g. H1:INJ"
        ),
        nargs="+",
        default={},
        action=DictionaryAction,
    )
    parser.add_argument(
        "--injection",
        help=(
            "A json file giving the injection parameters of a signal you "
            "wish to inject"
        ),
        default=None,
    )
    parser.add_argument(
        "--trigger_time",
        help=(
            "Either a GPS time or the event name you wish to analyse. If an "
            "event name is provided, GWOSC is queried to find the event time"
        ),
        default=None,
    )
    parser.add_argument(
        "--asd",
        help=(
            "ASD files to use for the analysis. Must be provided as a space "
            "separated dictionary, e.g. H1:path/to/file L1:path/to/file"
        ),
        nargs="+",
        default={},
        action=DictionaryAction,
    )
    parser.add_argument(
        "--psd",
        help=(
            "PSD files to use for the analysis. Must be provided as a space "
            "separated dictionary, e.g. H1:path/to/file L1:path/to/file"
        ),
        nargs="+",
        default={},
        action=DictionaryAction,
    )
    parser.add_argument(
        "--delta_f",
        help="Difference in frequency samples to use for PSD generation",
        default=0.0625,
        type=float
    )
    parser.add_argument(
        "--f_high",
        help=(
            "High frequency cutoff to use for the analysis. This also sets "
            "the sample rate (defined as f_high * 2)"
        ),
        default=8192.,
        type=float,
    )
    parser.add_argument(
        "--f_low",
        help=(
            "Lower frequency cutoff to use for the analysis. "
        ),
        default=20.,
        type=float,
    )
    parser.add_argument(
        "--multipoles",
        help=(
            "Multipoles to compute the injected SNR for. Must "
            "be speparated by a space e.g. 22 33 44"
        ),
        nargs="+",
        default=None,
    )
    parser.add_argument(
        "--seed",
        help="random seed to set for reproducibility",
        default=123456789,
        type=int
    )
    return parser


def get_gwosc_data(outdir, event_name, ifo):
    """Fetch GWOSC data with gwpy.timeseries.TimeSeries.fetch_open_data

    Parameters
    ----------
    outdir: str
        directory to output data
    event_name: str
        name of the event you wish to grab the data for
    ifo: str
        name of the IFO you wish to grab data for
    """
    from gwosc.datasets import event_gps
    import ast
    try:
        # check to see if a float is provided
        gps = ast.literal_eval(event_name)
        if not isinstance(gps, (float, int, np.number)):
            raise ValueError
    except ValueError:
        gps = event_gps(event_name)
    start, stop = int(gps) + 512, int(gps) - 512
    logger.info(
        f"Fetching strain data with: "
        f"TimeSeries.fetch_open_data({ifo}, {start}, {stop})"
    )
    open_data = TimeSeries.fetch_open_data(ifo, start, stop)
    _channel = "GWOSC"
    open_data.name = f"{ifo}:{_channel}"
    open_data.channel = f"{ifo}:{_channel}"
    os.makedirs(f"{outdir}/output", exist_ok=True)
    filename = f"{outdir}/output/{ifo}-{_channel}-{int(gps)}.gwf"
    logger.debug(f"Saving strain data to {filename}")
    open_data.write(filename)
    return filename, _channel


def get_injection_data(
    outdir, injection, ifo, psds, seed, gaussian_noise
):
    """Create an injection with pycbc and save to a gwf file

    Parameters
    ----------
    outdir: str
        directory to output data
    injection: dict
        a dictionary of the injection parameters
    ifo: str
        name of the IFO you wish to create data for
    """
    # make waveform with independent code: pycbc.waveform.get_td_waveform
    from pycbc.waveform import get_td_waveform, taper_timeseries
    from pycbc.detector import Detector

    injection_params = copy.deepcopy(injection)
    # convert to pycbc convention
    params_to_convert = [
        "mass_1", "mass_2", "spin_1x", "spin_1y", "spin_1z",
        "spin_2x", "spin_2y", "spin_2z"
    ]
    logger.info(f"Generating injection in {ifo} with parameters using pycbc:")
    for param, item in injection_params.items():
        logger.info(f"{param} = {item}")
    for param in params_to_convert:
        injection_params[param.replace("_", "")] = injection_params.pop(param)
    hp, hc = get_td_waveform(**injection_params)
    hp.start_time += injection_params["time"]
    hc.start_time += injection_params["time"]
    ra = injection_params["ra"]
    dec = injection_params["dec"]
    psi = injection_params["psi"]
    ht = Detector(ifo).project_wave(hp, hc, ra, dec, psi)
    ht = taper_timeseries(ht, tapermethod="TAPER_STARTEND")
    prepend = int(512 / ht.delta_t)
    ht.append_zeros(prepend)
    ht.prepend_zeros(prepend)
    strain = TimeSeries.from_pycbc(ht)
    strain = strain.crop(
        injection_params["time"] - 512, injection_params["time"] + 512
    )
    if gaussian_noise:
        from pycbc.noise import noise_from_psd
        logger.info("Adding gaussian-noise")
        noise = TimeSeries.from_pycbc(
            noise_from_psd(
                len(strain), strain.dt.value, psds[ifo], seed=seed
            )
        )
        noise.t0 = strain.t0
        strain = noise.inject(strain)
    strain.name = f"{ifo}:HWINJ_INJECTED"
    strain.channel = f"{ifo}:HWINJ_INJECTED"
    os.makedirs(f"{outdir}/output", exist_ok=True)
    filename = f"{outdir}/output/{ifo}-INJECTION.gwf"
    logger.debug("Saving injection to {filename}")
    strain.write(filename)
    return filename, "HWINJ_INJECTED"


def get_internal_data(outdir, trigger_time, ifo, channel):
    """Fetch data with gwpy.timeseries.TimeSeries.get

    Parameters
    ----------
    outdir: str
        directory to output data
    trigger_time: float
        central time to grab data for. By default, the start time is
        trigger_time-512 and end time if trigger_time+512
    ifo: str
        name of the IFO you wish to grab data for
    channel: str
        name of the channel you wish to grab data for
    """
    gps = float(trigger_time)
    start, stop = int(gps) - 512, int(gps) + 512
    logger.info(
        f"Fetching strain data with: "
        f"TimeSeries.get('{ifo}:{channel}', start={start}, end={stop}, "
        f"verbose=False, allow_tape=True,).astype(dtype=np.float64, "
        f"subok=True, copy=False)"
    )
    data = TimeSeries.get(
        f"{ifo}:{channel}", start=start, end=stop, verbose=False,
        allow_tape=True,
    ).astype(dtype=np.float64, subok=True, copy=False)
    filename = f"{outdir}/output/{ifo}-{channel}-{int(gps)}.gwf"
    logger.debug(f"Saving strain data to {filename}")
    data.write(filename)
    return filename, channel


def write_cache_file(outdir, strain, channels):
    """Write cache file

    Parameters
    ----------
    outdir: str
        directory to output data
    strain: dict
        dictionary containing the paths to the gwf files for each ifo
    channels: dict
        dictionary containing the channel names for each ifo
    """
    filename = f"{outdir}/output/strain_cache.json"
    logger.info(f"Saving cache file to: {filename}")
    with open(filename, "w") as f:
        _data = {
            ifo: {"strain": strain[ifo], "channel": f"{ifo}:{channels[ifo]}"}
            for ifo in strain.keys()
        }
        json.dump(_data, f)

def write_converted_injection_parameters(
    outdir, injection, psds, multipoles=None, f_low=20.0, f_high=1024.0, 
    delta_f=0.0625
):
    """Write the converted injection parameters, including 
    multipole and precessing network SNRs
    
    Parameters
    ----------
    outdir: str
        directory to output data
    injection: dict
        a dictionary of the injection parameters
    psds: dictionary 
        keys as the names of IFOs and values as psd filenames
    """
    from pycbc.waveform import get_fd_waveform
    from pycbc.filter import sigma
    from pycbc.detector import Detector

    injection_parameters = copy.deepcopy(injection)
    logger.info(
        "Computing multipole and precessing SNRs for injection "
        "parameters:"
    )
    if 'phase' not in list(injection_parameters):
        injection_parameters["phase"] = 0.0
    # updating
    injection_parameters["delta_f"] = delta_f
    injection_parameters["f_lower"] = f_low
    injection_parameters["f_final"] = f_high 
    # convert to pycbc convention
    params_to_convert = [
        "mass_1", "mass_2", "spin_1x", "spin_1y", "spin_1z",
        "spin_2x", "spin_2y", "spin_2z"
    ]
    for param in params_to_convert:
        param_name = param.replace("_", "")
        injection_parameters[param_name] = injection_parameters.pop(param)
    ifos = list(psds.keys())
    approximant = injection_parameters.pop("approximant")
    if multipoles is None:
        multipoles = ["22"]
        logger.info(
            "No multipoles parsed, computing SNR for the "
            "dominant 22 multipole"
        )
    for lm in multipoles:
        sigma_sq = 0.0
        injection_parameters["mode_array"] = waveform_modes.mode_array(
            lm, approximant
        )
        for ifo in ifos:
            fp, fc = Detector(ifo).antenna_pattern(
                injection_parameters["ra"], 
                injection_parameters["dec"], 
                injection_parameters["psi"], 
                injection_parameters["time"]
            )
            hp, hc = get_fd_waveform(
                approximant=approximant, **injection_parameters
            )
            h = hp*fp + hc*fc
            sigma_ifo = sigma(
                h, 
                psd=psds[ifo], 
                low_frequency_cutoff=injection_parameters["f_lower"],
                high_frequency_cutoff=injection_parameters["f_final"]
            )
            sigma_sq += sigma_ifo ** 2
        network_multipole_snr = np.sqrt(sigma_sq)
        injection_parameters[f"network_{lm}_snr"] = network_multipole_snr
        logger.info(
            f"Injected network SNR in {lm} multipole = {network_multipole_snr}"
        )
    for param, value in injection_parameters.items():
        injection_parameters[param] = [value]
    # convert to pesummary convention
    params_to_convert = {
        "mass1": "mass_1", "mass2": "mass_2", "spin1x": "spin_1x", 
        "spin1y": "spin_1y", "spin1z": "spin_1z", "spin2x": "spin_2x", 
        "spin2y": "spin_2y", "spin2z": "spin_2z", "inclination": "iota",
        "distance": "luminosity_distance", "time": "geocent_time"
    }
    for param, item in params_to_convert.items():
        injection_parameters[item] = injection_parameters.pop(param)
    samples = SimplePESamples(injection_parameters)
    samples.generate_all_posterior_samples(
        precessing_snr=True,
        disable_remnant=True,
        psd=psds,
        f_low=f_low,
        f_final=f_high,
        delta_f=delta_f
    )
    filename = f"{outdir}/output/converted_injection_parameters.json"
    logger.debug(
        "Saving converted injection parameters along multipole,"
        "and precessing SNRs"
    )
    samples_converted = {key: value[0] for key, value in samples.items()}
    with open(filename, "w") as f:
        json.dump(samples_converted, f, indent=4)


def main(args=None):
    """Main interface for `simple_pe_datafind`
    """
    parser = command_line()
    opts, _ = parser.parse_known_args(args=args)
    if not os.path.isdir(opts.outdir):
        os.mkdir(opts.outdir)
    _strain, _channels = {}, {}
    for ifo, value in opts.channels.items():
        if "gwosc" in value.lower():
            if opts.trigger_time is None:
                raise ValueError(
                    "Please provide the name of the event you wish to analyse "
                    "via the trigger_time argument"
                )
            _strain[ifo], _channels[ifo] = get_gwosc_data(
                opts.outdir, opts.trigger_time, ifo
            )
        elif "inj" in value.lower():
            if not os.path.isfile(opts.injection):
                raise FileNotFoundError(
                    f"Unable to find file: {opts.injection}"
                )
            with open(opts.injection, "r") as f:
                injection_params = json.load(f)
            # high frequency content in the psd used for generating 
            # gaussian noise is set by the sampling rate of the data
            if 'delta_t' in injection_params:
                f_high = 1 / 2 / injection_params["delta_t"]
            else:
                raise KeyError(
                    "delta_t must be passed to create an injection"
                )
            psds = io.load_psd_from_file(
                opts.psd, opts.asd, opts.delta_f, opts.f_low, f_high
            )
            _strain[ifo], _channels[ifo] = get_injection_data(
                opts.outdir, injection_params, ifo, psds, opts.seed, 
                opts.gaussian_noise
            )
        else:
            if opts.trigger_time is not None:
                _strain[ifo], _channels[ifo] = get_internal_data(
                    opts.outdir, opts.trigger_time, ifo, value
                )
            else:
                raise ValueError("Unable to grab strain data")
    write_cache_file(opts.outdir, _strain, _channels)

    if opts.injection:
        psds = io.load_psd_from_file(
            opts.psd, opts.asd, opts.delta_f, opts.f_low, opts.f_high
        )
        write_converted_injection_parameters(
            opts.outdir, injection_params, psds, opts.multipoles,
            opts.f_low, opts.f_high, opts.delta_f
        )


if __name__ == "__main__":
    main()
