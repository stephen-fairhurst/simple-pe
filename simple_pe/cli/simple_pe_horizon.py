#! /usr/bin/env python

import numpy as np
import pylab as plt
import os
import argparse
from pesummary.core.cli.actions import DictionaryAction

from pycbc import psd
from simple_pe import localization, detectors


def command_line():
    """
    Define the command line arguments for `simple_pe_horizon`
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--psd",
        help=(
            "PSD specification for each detector to use for the analysis. "
            "Available options given by psd.get_lalsim_psd_list()"
            "Must be provided as a space separated dictionary,"
            "e.g. aLIGO:aLIGOaLIGODesignSensitivityT1800044 "
            "CE:CosmicExplorerWidebandP1600143"
        ),
        nargs="+",
        default={},
        action=DictionaryAction,
    )
    parser.add_argument(
        "--f_low",
        help=(
            "low frequency cutoff to be used for each detector. "
            "Must be provided as a space "
            "separated dictionary, e.g. aLIGO:20 CE:5"
        ),
        nargs="+",
        default={},
        action=DictionaryAction,
    )
    parser.add_argument(
        "--min-mass",
        help="Minimum total mass for which to calculate horizon",
        default=2.0,
        type=float,
    )
    parser.add_argument(
        "--max-mass",
        help="Minimum total mass for which to calculate horizon",
        default=1e4,
        type=float
    )
    parser.add_argument(
        "--mass-ratio",
        help="value of mass ratio to use (can be more than one)",
        dest='qs',
        default=[],
        type=float,
        action='append',
    )
    parser.add_argument(
        "--horizon-snr",
        help="Signal to noise ratio to use when calculating horizon",
        default=8.0,
        type=float,
    )
    parser.add_argument(
        "--plot-psd",
        help="Plot the detector psds",
        action='store_true',
        default=False,
    )
    parser.add_argument(
        "--plot-detector-horizon",
        help="Plot the horizon in the detector frame",
        action='store_true',
        default=False,
    )
    parser.add_argument(
        "--plot-source-horizon",
        help="Plot the horizon in the detector frame",
        action='store_true',
        default=True,
    )
    parser.add_argument(
        "--outdir",
        help="Directory to store the output",
        default="./",
    )

    return parser


def main(args=None):
    """Main interface for `simple_pe_horizon`
    """
    parser = command_line()
    opts, _ = parser.parse_known_args(args=args)

    if not os.path.isdir(opts.outdir):
        os.mkdir(opts.outdir)

    dets = opts.psd.keys()
    if not opts.f_low or (opts.f_low.keys() != dets):
        raise ValueError(
            "Must specify f_low for each detector that has a psd"
        )

    # set detector colours for plotting
    prop_cycle = plt.rcParams['axes.prop_cycle']
    col = prop_cycle.by_key()['color']
    colors = {}
    for i, k in enumerate(dets):
        colors[k] = col[i]

    f_high = 2048.
    df = 1./256
    flen = int(f_high/df) + 1
    spin1 = 0
    spin2 = 0

    psds = {}
    for det in dets:
        opts.f_low[det] = float(opts.f_low[det])
        psds[det] = psd.analytical.from_string(opts.psd[det], flen, df,
                                               opts.f_low[det])

    # Plot the psds
    if opts.plot_psd:
        for k, p in psds.items():
            plt.loglog(p.sample_frequencies, p**0.5, label=k)
            plt.grid()
        plt.legend()
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('ASD /sqrt(Hz)')
        plt.xlim(3, 2000)
        plt.savefig('%s/%s_detector_asds.png' % (opts.outdir, "".join(dets)))

    # Calculate horizon vs detector mass
    m = np.logspace(np.log10(opts.min_mass), np.log10(opts.max_mass), 1000)
    horizon_detector = {}
    horizon_source = {}
    for q in opts.qs:
        horizon_detector[q] = {}
        horizon_source[q] = {}
        for det in dets:
            if det=='ET':
                tri=True
            else:
                tri=False
            horizon_detector[q][det] = \
                detectors.interpolate_horizon(opts.min_mass, opts.max_mass,
                                              q, spin1, spin2,
                                              psds[det], opts.f_low[det],
                                              opts.horizon_snr, triangle=tri)
            horizon_source[q][det] = \
                detectors.interpolate_source_horizon(opts.min_mass,
                                                     opts.max_mass,
                                                     horizon_detector[q][det],
                                                     1.)
            
    # make plots        
    if opts.plot_detector_horizon:
        m = np.logspace(np.log10(opts.min_mass), np.log10(opts.max_mass), 1000)

        for q in opts.qs:
            plt.figure(figsize=(10, 6))
            plt.title("Mass ratio: q=%1.f" % q)
            for det in dets:
                plt.loglog(m, horizon_detector[q][det](m), label=det,
                           color=colors[det])
            plt.grid(which='both')
            plt.legend()
            plt.xlim([opts.min_mass, opts.max_mass])
            plt.xlabel(r"Total mass detector ($M_\odot$) ")
            plt.ylabel("Horizon distance (Mpc)")
            plt.savefig("%s/%s_detector_horizon_q_%d.png" %
                        (opts.outdir, "".join(dets), q) )

    # calculate source frame horizon and shadings
    factors = {}
    factors['find10'] = 0.62
    factors['find50'] = 0.33
    find10 = {}
    find50 = {}
    for q in opts.qs:
        find10[q] = {}
        find50[q] = {}
        for det in dets:
            find10[q][det] = \
                detectors.interpolate_source_horizon(opts.min_mass,
                                                     opts.max_mass,
                                                     horizon_detector[q][det],
                                                     factors['find10'])
            find50[q][det] = \
                detectors.interpolate_source_horizon(opts.min_mass,
                                                     opts.max_mass,
                                                     horizon_detector[q][det],
                                                     factors['find50'])

    # make plots
    if opts.plot_source_horizon:
        for q in opts.qs:
            plt.figure(figsize=(10,6))
            plt.minorticks_on()
            plt.title("Mass ratio: q=%1.f" % q)
            for det in dets:
                plt.loglog(m, horizon_source[q][det](m),
                           label=det, color=colors[det])
                plt.fill_between(m, horizon_source[q][det](m),
                                 find10[q][det](m), alpha=0.7,
                                 color=colors[det])
                plt.fill_between(m, find10[q][det](m),
                                 find50[q][det](m), alpha=0.4,
                                 color=colors[det])
            plt.grid(which='both')
            plt.legend()
            plt.xlabel(r"Total mass detector ($M_\odot$) ")
            plt.ylabel("Horizon redshift (z)")
            plt.xlim([opts.min_mass, opts.max_mass])
            plt.ylim([0.1, 150])
            plt.savefig("%s/%s_horizon_q_%d.png" %
                        (opts.outdir, "".join(dets), q) )


if __name__ == "__main__":
    main()
