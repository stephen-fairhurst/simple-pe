import numpy as np
import astropy.units as u
import scipy.integrate as integrate
from astropy.cosmology import Planck18 as cosmo
from simple_pe.cosmology import cosmology

BNSrate = 105 * u.Gpc**-3 * u.yr**-1 
# From GWTC-3 population paper, p14 (fixed population)
# DOI: 10.1103/PhysRevX.13.011048

t_D_min = 0.02  # from Regimbau et. al https://journals.aps.org/prd/abstract/10.1103/PhysRevD.86.122001
t_D_max = cosmology.age_at_redshift(0)


def sfrMD(z):
    """
    Star formation rate as a function of redshift, based upon
    equation 15 on p. 48 of Madau and Dickenson (2014)
    http://www.annualreviews.org/doi/pdf/10.1146/annurev-astro-081811-125615
    uses (O_M, O_de, h) = (0.3, 0.7, 0.7) params

    :param z: The redshift(s)
    :type z: np.array, required

    :return: star formation rate (in Solar masses per Mpc^3 year) at given
     redshifts
    :rtype: np.array
    """
    sfrmd = 0.015*(1.+z)**2.7/(1.+((1.+z)/2.9)**5.6)  # msun per yr per Mpc^3
    return sfrmd


def simple_sfr(z):
    """
    Simple star formation model that is close to Madau Dickenson, but uses
    simple functions of lookback time

    :param z: Redshifts
    :type z: np.array, required

    :return: star formation rate (in Solar masses per Mpc^3 year)
     at given redshifts
    :rtype: np.array
    """
    t = cosmo.lookback_time(z)
    sfr = np.zeros_like(z)

    t0 = 10.
    t1 = 11.7
    tlow = t.value < t0
    tmid = (t.value > t0) & (t.value < t1)
    thi = (t.value > t1)

    sfr[tlow] = 0.014 * np.power(10, t.value[tlow]/10)
    sfr[tmid] = np.ones_like(t.value)[tmid] * 0.14
    sfr[thi] = 0.14 * np.power(10, (t1 - t.value[thi]))

    return sfr


def sfrHB(z):
    """
    Star formation rate as a function of redshift, based upon
    Hopkins and Beacom (2006)
    http://iopscience.iop.org/article/10.1086/506610/pdf

    :param z: The redshift(s)
    :type z: np.array, required

    :return: star formation rate (in Solar masses per Mpc^3 year)
     at given redshifts
    :rtype: np.array
    """
    return 0.7 * (0.017 + 0.13*z) / (1.+(z/3.3)**5.3)


def sfr2_porciani_madau(z):
    """
    Star formation rate as a function of redshift, based upon
    SFR2 from porciani madau 2001,
    http://stacks.iop.org/0004-637X/548/i=2/a=522

    :param z: The redshift(s)
    :type z: np.array, required

    :return: star formation rate (in Solar masses per Mpc^3 year)
     at given redshifts
    :rtype: np.array
    """
    return np.exp(3.4 * z) / (np.exp(3.4 * z) + 22.) * \
        cosmo.efunc(z) / ((1 + z)**(3/2.))


def delay_distribution(t_D, t_D_min=t_D_min,
                       t_D_max=cosmology.age_at_redshift(0)):
    """
    Time delay probability assuming 1/t delay time distribution

    :param t_D: Array of time delays
    :type t_D: np.array, required
    :param t_D_min: Minimum allowed time delay
    :type t_D_min: np.array, optional
    :param t_D_max: Minimum allowed time delay
    :type t_D_max: np.array, optional

    :return: probability for each input time delay assuming
     1/t time delay distribution
    :rtype: np.array
    """
    if isinstance(t_D, u.quantity.Quantity):
        t_D = t_D.value
    # denominator = integral of 1/t from min to max delay times.
    norm_cnst = 1/np.log(t_D_max/t_D_min)

    if isinstance(t_D, np.ndarray):
        test_dist = 1./t_D
        test_dist[np.where(t_D_max < t_D)] = 0
        test_dist[np.where(t_D < t_D_min)] = 0
        return norm_cnst*test_dist
    else:
        # when times are given as floats, instead of arrays
        if t_D < t_D_min:
            return 0
        elif t_D > t_D_max:
            return 0
        else:
            return norm_cnst*1/t_D


def z_at_formation(z, t_D):
    """
    takes the redshift of a source at merger and the delay since formation,
    outputs the redshift at which the binary was formed

    :param z: The redshift(s) at merger
    :type z: np.array, required
    :param t_D: delay time between formation and merger of binary
    :type t_D: np.array, required

    :return: redshift at formation
    :rtype: np.array
    """
    age = cosmology.age_at_redshift(z) - t_D
    if (age < 0).any():
        raise ValueError('this time delay - redshift combination would '
                         'require formation '
                         'before the beginning of the universe')
    else:
        return cosmology.redshift_at_age(age)


def rate_density_integrand(tb, t, sfr=sfrMD):
    """
    The merger rate density as given in

    - eq 37 of Nakar (2007)
      http://www.sciencedirect.com/science/article/pii/S0370157307000476?via%3Dihub
    - equivalent to eq 3 of LIGO GW150914 stochastic paper (2016)
      https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.116.131102
    - eq 1 from Anand (2017) https://arxiv.org/pdf/1710.04996.pdf
    - eq 3 of Ghirlanda (2016)
      https://www.aanda.org/articles/aa/full_html/2016/10/aa28993-16/aa28993-16.html
    - eq 1 of LIGO kilanova paper (2017)
      http://iopscience.iop.org/article/10.3847/2041-8213/aa9478/pdf
    - eq 18 in Taylor and Gair (2012) https://arxiv.org/abs/1204.6739

    :param tb: the birth time of the binary
    :type tb: np.array, required
    :param t: the merger time of the binary
    :type t: np.array, optional
    :param sfr:  function to use for the star formation rate
    :type sfr: function, optional

    :return: rate density integrand
    :rtype: np.array
    """
    t_D = t - tb
    zf = cosmology.redshift_at_age(tb)
    return sfr(zf) * delay_distribution(t_D)


def rate_density(z, z_max=20, sfr=sfrMD, epsrel=1e-4):
    """
    Computes the rate density at z by integrating over the contributions
    from all possible delay times from binary birth to merger
    and the attending SFR at the at the time of formation

    :param z: The redshift at merger
    :type z: float, required
    :param z_max: The maximum redshift to integrate to
    :type z_max: float, optional
    :param sfr:  function to use for the star formation rate
    :type sfr: function, optional
    :param epsrel: the relative error allowed on the integral
    :type epsrel: float, optional

    :return: the rate density (integrated over possible time delays)
    :rtype: float
    """
    # otherwise rounding errors causes astropy to throw an error
    epsilon = 0.001
    t = cosmology.age_at_redshift(z) - epsilon
    t_min = cosmology.age_at_redshift(z_max)
    return integrate.quad(rate_density_integrand, t_min, t,
                          args=(t, sfr), epsrel=epsrel,
                          epsabs=0)[0]


def rate_at_redshift(z, r0=BNSrate, z_max=20, sfr=sfrMD):
    """
    Calculate the observed rate of mergers at a given redshift,
    using the specified local merger rate and the given star formation
    rate function.  This is eq 31 in Regimbau 2012,
    https://journals.aps.org/prd/pdf/10.1103/PhysRevD.86.122001

    :param z: the redshift
    :type z: float, required
    :param r0: the merger rate at z=0
    :type r0: float, optional
    :param z_max: the maximum redshift to consider
    :type z_max: float, optional
    :param sfr: star formation rate function
    :type sfr: function, optional

    :return: the rate for given z, integrated over delay times
    :rtype: float
    """
    norm = r0 / rate_density(0, z_max, sfr) * u.yr
    R_local = norm * rate_density(z, z_max=z_max, sfr=sfr)
    # note that differential_comoving_volume is given per steradian,
    # so we multiply by 4 pi
    dv_dz = 4 * np.pi * u.sr * \
        cosmo.differential_comoving_volume(z).to(u.Gpc**3 / u.sr)
    # observed rate scales with 1/(1+z)
    return (dv_dz * R_local * 1/(1+z)).value


def rate_density_coalescence_integrand(t_D, z, sfr=sfrMD):
    """
    The rate density for coalescence, accounting for additional (1 + z)^-1
    where z is the redshift at formation

    :param t_D: delay time
    :type t_D: float, required
    :param z: redshift
    :type z: float, required
    :param sfr: star formation rate function
    :type sfr: function, optional

    :return: the rate density at the given time delay and redshift
    :rtype: float
    """
    zf = z_at_formation(z, t_D)
    return sfr(zf) * delay_distribution(t_D) / (1 + zf)


def rate_density_coalescence(z, t_D_min=t_D_min, t_D_max=t_D_max, epsrel=1e-4):
    """
    Calculte the rate integrated over all possible time delays

    :param z: redshift
    :type z: float, required
    :param t_D_min: Minimum allowed time delay
    :type t_D_min: np.array, optional
    :param t_D_min: Minimum allowed time delay
    :type t_D_min: np.array, optional
    :param epsrel: the relative error allowed on the integral
    :type epsrel: float, optional

    :return: the rate density at a given redshift, integrated over delay times
    :rtype: float
    """
    epsilon = 0.001
    # otherwise rounding errors causes astropy to throw an error
    t_D_max = cosmology.age_at_redshift(z) - epsilon
    return integrate.quad(rate_density_coalescence_integrand, t_D_min, t_D_max,
                          args=(z), epsrel=epsrel, epsabs=0)[0]


def coalescence_rate_at_redshift(z, r0=BNSrate):
    """
    eq 31 in Regimbau 2012,
    https://journals.aps.org/prd/pdf/10.1103/PhysRevD.86.122001

    :param z: redshift
    :type z: float, required
    :param r0: the merger rate at z=0
    :type r0: float, optional

    :return: the rate integrand as a function of z
    :rtype: float
    """
    norm = r0 / rate_density_coalescence(0)
    R_local = norm * rate_density_coalescence(z)
    # note that differential_comoving_volume is given per steradian,
    # so we multiply by 4 pi
    dv_dz = 4 * np.pi * u.sr * \
        cosmo.differential_comoving_volume(z).to(u.Gpc**3 / u.sr)
    return dv_dz * R_local * u.yr
