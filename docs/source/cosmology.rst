Cosmology package
=================

simple_pe.cosmology.cosmology module
------------------------------------

.. automodule:: simple_pe.cosmology.cosmology
    :members:
    :undoc-members:
    :show-inheritance:

simple_pe.cosmology.merger_rate_evolution module
------------------------------------------------

.. automodule:: simple_pe.cosmology.merger_rate_evolution
    :members:
    :undoc-members:
    :show-inheritance:
