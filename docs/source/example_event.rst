Analysing a GW event
====================
.. _real_event_example:

For a brief overview of the analysis and results on a simulated signal, see
:ref:`zero_noise_injection`.

Here, we analyze a real GW event using the :code:`simple_pe_gracedb` and
:code:`simple_pe_pipe` executables. For concreteness, we use events from
ER15: S230518h and S230522a (we include the second example as it was
observed in only on detector).

:code:`simple_pe_pipe` requires command line arguments to run. Out of
convenience, these command line arguments can be provided from a
configuration file, and obtained by running :code:`simple_pe_gracedb`. 

Example: S230518h
-----------------

This is a two detector event with the two LIGO Observatories, H1 and L1
operational at the time.

To set up the analysis, run :code:`simple_pe_gracedb` with the following arguments

.. code-block:: bash

    simple_pe_gracedb --sid S230518h --outdir ./outdir

This will download all required data from GraceDB and generate the following
configuration file:

.. code-block:: ini

    [pesummary]
    trigger_time = 
    channels = {H1:GDS-CALIB_STRAIN_CLEAN,L1:GDS-CALIB_STRAIN_CLEAN}
    outdir = ./outdir
    f_low = 20.0
    f_high = 2048.0
    trigger_parameters = {}
    approximant = IMRPhenomXPHM
    psd = {H1:./outdir/gracedb/H1_psd.txt,L1:./outdir/gracedb/L1_psd.txt}
    minimum_data_length = 256
    snr_threshold =
    peak_finder = grid
    convert_samples = True
    accounting_group =
    accounting_group_user = 

.. note::

    Certain information has been removed for confidentiality

We see that the minimum data length is set to 256s. This is must 
be set to be long enough to handle any type of signal without truncation
of the waveform.  In optimizing the SNR, the code explores the parameter
space and may generate BNS waveforms.  If the event you are running is
definitely a BBH merger, then you may find that reducing the minimum data
length speeds up the analysis.

To launch this analysis, simply run:

.. code-block:: bash

    simple_pe_pipe ./outdir/config.ini

This will produce the simple-pe results for the event.


Example: S230522a
-----------------

This is a one detector event with the L1 Observatory reported as being
operational at the time of the event.

To set up the analysis, run :code:`simple_pe_gracedb` with the following
arguments

.. code-block:: bash

    simple_pe_gracedb --sid S230522a  --outdir ./outdir

As before, this will download all required data from GraceDB and generate
the following configuration file:

.. code-block:: ini

    [pesummary]
    trigger_time = 
    channels = {L1:GDS-CALIB_STRAIN_CLEAN}
    outdir = ./outdir
    f_low = 20.0
    f_high = 2048.0
    trigger_parameters = {}
    approximant = IMRPhenomXPHM
    psd = {L1:./outdir/gracedb/L1_psd.txt}
    minimum_data_length = 256
    snr_threshold = 
    peak_finder = grid
    convert_samples = True
    accounting_group =
    accounting_group_user =

As before, this job can be launched by running:

.. code-block:: bash

    simple_pe_pipe ./outdir/config.ini
