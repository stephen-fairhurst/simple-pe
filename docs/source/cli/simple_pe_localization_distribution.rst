===================================
simple_pe_localization_distribution
===================================

The `simple_pe_localization_distribution` executable generates a set of
binary merger events, uniformly distributed in sky location and orientation 
(assuming a uniform in volume distribution of events).  For each event, it 
determines whether the event would be observed in the given network and, if 
so, calculates the localization area for the event using the specified 
localization method.  

The localization can be performed in one of several different ways:

* 'time': using time delays between detectors only
* 'coh': requiring a coherent signal between the detectors
* 'left': assuming that the signal is left circular polarized
* 'right': assuming that the signal is right circular polarized
* 'marg': marginalizing the likelihood over distance and orientation to determe which of
  ('coh','left','right') gives the highest likelihood and use that in localization.

   
 To see help for this executable please run:

.. code-block:: console

    $ simple_pe_localization_distribution --help

.. program-output:: simple_pe_localization_distribution --help