#!/usr/bin/env python

import glob
import os
import json
import h5py
import numpy as np
import argparse

__authors__ = [
    "Charlie Hoy <charlie.hoy@ligo.org>",
]
__doc__ = """Workflow generator to compare the outputs from simple-pe with
the published GWTC results
"""

parser = argparse.ArgumentParser(
    description=__doc__
)
parser.add_argument(
    "--conda_env",
    help="conda environment to use. Default $CONDA_PREFIX",
    default=os.environ.get("CONDA_PREFIX", "."),
)
parser.add_argument(
    "--accounting_group_user",
    help="Accounting group user to use for this workflow",
    required=True
)
parser.add_argument(
    "--accounting_group",
    help="Accounting group to use for this workflow",
    required=True
)
parser.add_argument(
    "--outdir",
    help="Directory to store the output from this workflow. Default './'",
    default="./"
)
parser.add_argument(
    "--webdir",
    help="Directory to store comparison PESummary webpages",
    required=True
)
parser.add_argument(
    "--name_map",
    help="JSON file giving a map from GW name to SID name",
    default="names.json"
)
parser.add_argument(
    "--focus",
    help=(
        "GW events to analyse. If empty, all events in O1, O2 and O3 are "
        "analysed. Default 'GW150914 GW190412_053044 GW190814_211039'"
    ),
    default=["GW150914", "GW190412_053044", "GW190814_211039"],
    nargs="+",
)
opts = parser.parse_args()

config = """[pesummary]
channels={%s}
minimum_data_length=256
trigger_time=%s
outdir=%s
peak_finder=grid

psd={%s}
trigger_parameters={mass_1: %s, mass_2: %s, spin_1z: %s, spin_2z: %s, time: %s, chi_p:0.1}

accounting_group=%s
accounting_group_user=%s

approximant=IMRPhenomXPHM
f_low=20
f_high=2048
convert_samples=True
"""
cwd = os.getcwd()
os.chdir(opts.outdir)
complete_dag = "multidag.dag"
dag_obj = open(complete_dag, "w")

with open(opts.name_map, "r") as f:
    mapping = json.load(f)

_base = "/home/pe.o3/"
reverse_mapping = {item: key for key, item in mapping["GRACEDBSID"].items()}
O1O2O3a_events = glob.glob(f"{_base}/o3a_final/reviewed/sid/*")
O3b_events = glob.glob("{_base}/o3b_catalog/reviewed/sid/S*")
all_events = []
all_events_sid = [_.split("/")[-1] for _ in O1O2O3a_events + O3b_events]
all_events_full = []
for sid in all_events_sid:
    gracedb = reverse_mapping.get(sid, sid)
    full = mapping["FULLNAME"].get(gracedb, gracedb)
    all_events_full.append(full)

for num, event in enumerate(all_events_full):
    if len(opts.focus) and event not in opts.focus:
        continue
    os.makedirs(event, exist_ok=True)
    released_posterior = (
        f"{_base}/o3a_final/reviewed/sid/{all_events_sid[num]}/"
        f"mixed-nocosmo.h5"
    )
    if not os.path.isfile(released_posterior):
        released_posterior = (
            f"{_base}/o3b_catalog/reviewed/sid/{all_events_sid[num]}/"
            f"mixed-nocosmo.h5"
        )
    with h5py.File(released_posterior, "r") as f:
        if "C01:IMRPhenomXPHM" not in f.keys():
            print(
                f"Unable to find IMRPhenomXPHM analysis for {event}. "
                f"Skipping"
            )
            continue
        psd = f["C01:IMRPhenomXPHM"]["psds"]
        ifos = list(psd.keys())
        for ifo in ifos:
            _freqs = psd[ifo][:][:, 0]
            _psd = psd[ifo][:][:, 1]
            np.savetxt(
                f"{event}/psd_{ifo}.txt", np.array([_freqs, _psd]).T
            )
        posterior = f["C01:IMRPhenomXPHM"]["posterior_samples"]
        ind = np.argmax(posterior["log_likelihood"][:])
        m1 = np.round(posterior["mass_1"][ind], 2)
        m2 = np.round(posterior["mass_2"][ind], 2)
        s1z = np.round(posterior["spin_1z"][ind], 2)
        s2z = np.round(posterior["spin_2z"][ind], 2)
        time = np.round(posterior["geocent_time"][ind], 3)
    with open(f"{event}/config.ini", "w") as f:
        _config = config % (
            ",".join([f"{ifo}:GWOSC" for ifo in ifos]),
            time,
            f"{event}/output",
            ",".join([f"{ifo}:{event}/psd_{ifo}.txt" for ifo in ifos]),
            m1, m2, s1z, s2z, time,
            opts.accounting_group, opts.accounting_group_user
        )
        f.write(_config)
    os.system(f"simple_pe_pipe {event}/config.ini")
    os.system(
        f"summaryextract --samples {released_posterior} --file_format dat "
        f"--outdir {event} --filename released_IMRPhenomXPHM_samples.dat "
        f"--label C01:IMRPhenomXPHM"
    )
    external_dag = glob.glob(f"{event}/output/submit/*.dag")[-1]
    dag_obj.writelines(
        [f"SUBDAG EXTERNAL {event} {external_dag}\n"]
    )
    dag_obj.writelines(
        [f"JOB compare_{event} compare_pesummary.sub\n"]
    )
    dag_obj.writelines([
        f"VARS compare_{event} "
        f"env=\"{opts.conda_env}/bin\" "
        f"webdir=\"{opts.webdir}/{event}\" "
        f"GWTC=\"{event}/released_IMRPhenomXPHM_samples.dat\" "
        f"simple_pe=\"{event}/output/output/converted_posterior_samples.dat\" "
        f"outdir=\"{event}/output\" "
        f"accounting_group=\"{opts.accounting_group}\" "
        f"accounting_group_user=\"{opts.accounting_group_user}\"\n"
    ])
    dag_obj.writelines(
        [f"Parent {event} Child compare_{event}\n"]
    )

dag_obj.close()
os.system(
    f"cp {cwd}/compare_pesummary.sub {opts.outdir}/compare_pesummary.sub"
)
os.chdir(cwd)
